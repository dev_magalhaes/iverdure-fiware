public class attributesCuttingMode
{
    public char description { get; set; }
    public float value_multiplier { get; set; }
    public bool only_int { get; set; }
}
public class CuttingMode
{
    public int id { get; set; }
    public string type = "CuttingMode";
    public bool isPattern = false;
    public attributesCuttingMode attributes { get; set; }
    public CuttingMode()
    {
        this.id = 0;
        this.attributes = new attributesCuttingMode
        {
            description = 'E',
            value_multiplier = 9.9f,
            only_int = false
        };
    }
}
public class attributesFood
{
    public string name { get; set; }
    public int cutting_mode_id { get; set; }
    public float price { get; set; }
    public float amount { get; set; }
    public CuttingMode CuttingMode { get; set; }
}
public class Food
{
    public int id { get; set; }
    public string type = "Food";
    public bool isPattern = false;
    public attributesFood attributes { get; set; }
    public Food()
    {
        this.id = 0;
        this.attributes = new attributesFood
        {
            name = "Example",
            price = 9.9f,
            amount = 9.9f,
            cutting_mode_id = 0,
            CuttingMode = new CuttingMode()
        };
    }
}

public class attributesOrder
{
    public int client_idd { get; set; }
    public ICollection<Food> Foods { get; set; }
}

public class Order
{
    public int id { get; set; }
    public string type = "Order";
    public bool isPattern = false;
    public attributesOrder attributes { get; set; }
    public Order()
    {
        this.id = 1;
        this.attributes = new attributesOrder {
            client_idd = 1,
            Foods = new List<Food> {
                new Food {
                    id = 1,
                    attributes = new attributesFood {
                        name = "Abacate",
                        cutting_mode_id = 1,
                        price = 4.5f,
                        amount = 1 ,
                        CuttingMode = new CuttingMode {
                            id = 1,
                            attributes = new attributesCuttingMode {
                                description = 'U',
                                value_multiplier = 1.1f,
                                only_int = true
                            }
                        }
                    }
                }
            }
        };
    }
}
